// Componentes
Vue.component('header-master', {
    template: `
        <div>
            <nav class="navbar navbar-dark bg-dark d-flex align-items-center shadow-sm header_master">
              <a href="#" class="text-white" @click.prevent="SHOW_SIDEBAR">
                <i class="fa fa-bars fa-lg"></i>
              </a>
              <a class="navbar-brand mx-auto" href="#">DAT</a>
              <a href="#" @click.prevent="$parent.$parent.LOGOUT" class="text-white">
                <i class="fa fa-power-off fa-lg"></i>
              </a>
            </nav>
            <aside class="bg-dark text-white sidebar">
                <p class="m-0 mb-3 border-bottom border-dark pb-3 lead">
                    {{ $parent.$parent.user.name }}
                </p>
            </aside>
        </div>
    `,
    mounted() {
        this.HIDE_SIDEBAR();
    },
    methods: {
        SHOW_SIDEBAR() {
            var t = this;
            var sidebar = $('.sidebar');
            if (sidebar.hasClass('active')) {
                sidebar.removeClass('active');
            } else {
                setTimeout(function() {
                    sidebar.addClass('active');
                }, 50);
            }
        },
        HIDE_SIDEBAR() {
            $(document).on('click', function(e) {
                var sidebar = $('.sidebar');
                if (sidebar.hasClass('active')) {
                    if (!sidebar.is(e.target) && sidebar.has(e.target).length === 0) {
                        sidebar.removeClass('active');
                    }
                }
            });
        }
    }
});
Vue.component('create-button', {
    template: `
        <a href="#" class="btn btn-primary create_btn">
            <i class="fa fa-plus"></i>
        </a>
    `,
});
Vue.component('back-bar', {
    props: ['src'],
    template: `
        <section class="bg-white px-3 py-3 d-flex align-items-center">
            <a href="#" class="text-dark btn btn-link p-0 text-uppercase btn-sm" @click.prevent="$parent.$parent.currentView = src">
                <b><i class="fa fa-angle-left fa-lg"></i> Back</b>
            </a>
            <div class="ml-auto">
                <slot name="toolbar"></slot>
            </div>
        </section>
    `
});
Vue.component('preload', {
    template: `
        <div class="py-5 text-center">
            <i class="fa fa-spin fa-spinner fa-lg"></i>
        </div>
    `
});

// Vistas
var loginForm = {
    template: `
        <section class="h-100 w-100 d-flex align-items-center justify-content-center">
            <div class="container-fluid text-center">
                <form class="form-signin w-100" @submit.prevent="GET_TOKEN" method="POST">
                    <h1 class="h3">Welcome to DAT</h1>
                    <p>Please sign in to continue</p>
                    <div id="login_form_alerts"></div>
                    <div class="form-group">
                        <input type="email" name="email" placeholder="Email address" class="form-control form-control-lg" required/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="Password" class="form-control form-control-lg" required/>
                    </div>
                    <button type="submit" class="btn btn-dark btn-block btn-lg">Login</button>
                    <p class="m-0 mt-3 small">©2019, DAT - Rights Reserved.</p>
                </form>
            </div>
        </section>
    `,
    methods: {
        GET_TOKEN(e) {
            var t = this;
            var form = $(e.target);
            var route = t.$parent.root + 'oauth/token';
            var btn = $('button', form);
            var data = {
                username: $('[name="email"]', form).val(),
                password: $('[name="password"]', form).val(),
                grant_type: 'password',
                client_secret: 'aJgZnMmbkFXqvcBFxwSAKKv0wxwUMB2EldQBpPa3',
                client_id: 2,
            }
            form.find('.alert').remove();
            btn.attr('disabled', 'disabled');
            this.$http.post(route, data, {
                headers: {
                    'Accept': 'application/json'
                }
            }).then(function(response) {
                function SAVE_ACCESS_TOKEN() {
                    t.$parent.ls.setItem('DAT_ACCESS_TOKEN', 'Bearer ' + response.body.access_token);
                }
                $.when(SAVE_ACCESS_TOKEN()).then(t.$parent.LOGIN());
            }, function(response) {
                var danger_alert = `
                    <div class="alert alert-danger mb-3 small" role="alert">
                      ` + response.body.message + `
                    </div>
                `;
                form.find('#login_form_alerts').prepend(danger_alert);
                btn.removeAttr('disabled');
            });
        }
    }
}
var dashboard = {
    template: `
        <section>
            <header-master></header-master>
            <div class="container-fluid bg-light py-3 bg-light border-0">
                <form action="">
                    <input type="text" class="form-control form-control-lg" placeholder="Search...">
                </form>
            </div>
            <div class="list-group list-group-flush">
                <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                    <span>Accounts</span>
                    <span class="ml-auto">({{$parent.totals.accounts}})</span>
                </a>
                <a href="#" @click.prevent="$parent.currentView = 'assets-list'" class="list-group-item list-group-item-action d-flex align-items-center">
                    <span>Assets</span>
                    <span class="ml-auto">({{$parent.totals.assets}})</span>
                </a>
                <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                    <span>Users</span>
                    <span class="ml-auto">({{$parent.totals.users}})</span>
                </a>
            </div>
            <create-button></create-button>
        </section>
    `,
}
var assetsList = {
    template: `
        <section>
            
            <header-master></header-master>
            
            <back-bar src="dashboard">
                <template slot="toolbar">
                    <a href="#" class="btn btn-link p-0 text-dark" data-toggle="collapse" data-target="#filterAssets">
                        <i class="fa fa-search fa-sm"></i>
                    </a>
                </template>
            </back-bar>

            <div id="filterAssets" class="collapse">
                <div class="p-3">
                    <div class="form-group form-row">
                        <label for="" class="col-3 col-form-label text-right">Type:</label>
                        <div class="col-7">
                            <select name="type_id" class="form-control">
                                <option disabled selected value="">Choose an option</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group form-row">
                        <label for="" class="col-3 col-form-label text-right">Model:</label>
                        <div class="col-7">
                            <select name="type_id" class="form-control">
                                <option disabled selected value="">Choose an option</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group form-row">
                        <div class="col-7 offset-3">
                            <button type="submit" class="btn btn-primary btn-block">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <ul class="list-group list-group-flush" v-if="$parent.assets">
                <li v-for="asset in $parent.assets.data" class="list-group-item d-flex align-items-center" @click.prevent="SHOW_ASSET(asset.slug)">
                    <div class="mr-2">
                        <p class="m-0"><b>{{ asset.type.name }} | {{ asset.brand.name }} | {{ asset.model.name }}</b></p>
                        <p class="small m-0">{{ asset.account.name }} | {{ asset.department.name }}</p>
                        <p class="small m-0 text-muted">{{ asset.employee.full_name }}</p>
                    </div>

                    <a href="#" class="text-dark ml-auto">
                        <i class="fa fa-angle-right fa-lg"></i>
                    </a>
                </li>
            </ul>
            
            <div v-else>
                <preload></preload>
            </div>
            
        </section>
    `,
    mounted() {
        this.GET_ASSETS();
    },
    methods: {
        GET_ASSETS() {
            var t = this;
            var route = t.$parent.root + 'api/assets';
            t.$http.get(route, {
                headers: {
                    Authorization: t.$parent._TOKEN(),
                    Accept: 'application/json'
                }
            }).then(response => {
                t.$parent.assets = response.body;
            });
        },

        SHOW_ASSET(asset_id){

            var t = this;

            t.$parent.asset = asset_id;

            t.$parent.currentView = 'asset-show';

        }
    }
}
var assetShow = {

    template: `
        <section>
            
            <header-master></header-master>
            
            <back-bar src="assets-list">
                <template slot="toolbar">
                    <a href="#" class="btn btn-link text-dark p-0">
                        <i class="fa fa-pencil-alt fa-sm"></i>
                    </a>
                </template>
            </back-bar>
            
            <div v-if="asset">
                
                <table class="table table-striped text-center table-sm">
                    <tr>
                        <th>{{ asset.type.name }} | {{ asset.brand.name }} | {{ asset.model.name }}</th>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-phone"></i> 
                            {{ asset.phone_number }}
                            <span v-if="asset.extension_number">Ext. {{ asset.extension_number }}</span>

                        </th>
                    </tr>
                    <tr>
                        <th><i class="fa fa-user"></i> {{ asset.employee.full_name }}</th>
                    </tr>
                </table>



            </div>
    
            <div v-else>
                <preload></preload>
            </div>

        </section>
    `,

    data(){

        return {

            asset: null

        }

    },

    mounted(){

        this.GET_ASSET();

    },

    methods: {

        GET_ASSET(){

            var t = this;

            var route = t.$parent.root + 'api/assets/' + t.$parent.asset;

            t.$http.get(route, {
                headers: {
                    Authorization: t.$parent._TOKEN(),
                    Accept: 'application/json'
                }
            }).then(response => {
                t.asset = response.body;
            });

        }

    }

}

// Aplicacion
var fullApp = new Vue({
    el: '#vue_app',
    data: {
        ls: localStorage,
        root: 'http://dat.dynacomus.com/',
        user: null,
        currentView: '',
        totals: {
            assets: 0,
            acccouts: 0,
            users: 0
        },
        assets: null,
        asset: null
    },
    components: {
        'login-form': loginForm,
        'dashboard': dashboard,
        'assets-list': assetsList,
        'asset-show': assetShow
    },
    mounted() {
        var t = this;
        t.CHECK_AUTH();
    },
    methods: {
        _TOKEN() {
            return this.ls.getItem('DAT_ACCESS_TOKEN') ? this.ls.getItem('DAT_ACCESS_TOKEN') : null;
        },
        CHECK_AUTH() {
            var t = this;
            if (t._TOKEN()) {
                t.LOGIN();
            } else {
                t.currentView = 'login-form';
            }
        },
        LOGIN() {
            var t = this;
            var route = t.root + 'api/user';
            this.$http.get(route, {
                headers: {
                    Authorization: t._TOKEN(),
                    Accept: 'application/json'
                }
            }).then(function(response) {
                t.user = response.body.user;
                t.totals.assets = response.body.total_assets;
                t.totals.accounts = response.body.total_accounts;
                t.totals.users = response.body.total_users;
                t.currentView = 'dashboard';
            }, function() {
                t.ls.removeItem('DAT_ACCESS_TOKEN');
                t.currentView = null;
                t.user = null;
                t.CHECK_AUTH();
            });
        },
        LOGOUT() {
            var t = this;
            var route = t.root + 'api/logout';
            t.$http.get(route, {
                headers: {
                    Authorization: t._TOKEN(),
                    Accept: 'application/json'
                }
            }).then(response => {
                if (response.body.type == 'success') {
                    t.ls.removeItem('DAT_ACCESS_TOKEN');
                    t.currentView = '';
                    t.user = null;
                    t.CHECK_AUTH();
                }
            });
        }
    }
});